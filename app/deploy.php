<?php
namespace Deployer;

use Deployer;
require 'recipe/common.php';
//require 'vendor/deployer/recipes/recipe/telegram.php';

// Project name
set('application', 'henriqueserrat.com');

// Project repository
set('repository', 'git@bitbucket.org:rickserrat/seugeek.git');

// [Optional] Allocate tty for git clone. Default value is false.
set('git_tty', true);

// Shared files/dirs between deploys
set('shared_files', []);
set('shared_dirs', []);

// Writable dirs by web server
set('writable_dirs', []);
set('allow_anonymous_stats', false);

// Hosts

host('production')
    ->hostname('henriqueserrat.com')
    ->stage('production')
    ->user('rickserrat')
//  ->port('22')
    ->set('deploy_path', '/var/www/henriqueserrat.com/htdocs/pokeapi')
    ->set('branch', 'master');


// Tasks

desc('Deploy your project');
task('deploy', [
    'deploy:info',
    'deploy:prepare',
    'deploy:lock',
    'deploy:release',
    'deploy:update_code',
    'deploy:shared',
    'deploy:writable',
    'deploy:vendors',
    'deploy:clear_paths',
    'deploy:symlink',
    'deploy:unlock',
    'cleanup',
//    'telegram:notify',
//    'telegram:notify:success',
    'success'
]);




//before('deploy', 'telegram:notify');
//after('success', 'telegram:notify:success');


// [Optional] If deploy fails automatically unlock.
after('deploy:failed', 'deploy:unlock');
