<?php

namespace App\Controllers;
use PokePHP\PokeApi;

class PokeController
{
    /**
     * @param $baseUrl
     * @param $endpoint
     * @param null $limit
     * @param null $offset
     * @return bool|false|string
     */

    protected $baseUrl = 'https://pokeapi.co/api/v2';


    public function resourceList($endpoint, $limit = null, $offset = null)
    {
        $url = $this->baseUrl.'/'.$endpoint.'/?limit='.$limit.'&offset='.$offset;
        return $this->sendRequest($url);
    }

    public function pokemon($name)
    {
        //https://pokeapi.co/api/v2/pokemon/;namePokemon/

        $url = $this->baseUrl.$name;
        return $this->sendRequest($url);
    }

    /**
     * @param string $url
     * @return bool|false|string
     */
    public function sendRequest($url)
    {
        $ch = curl_init();
        $timeout = 5;
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
        $data = curl_exec($ch);
        $http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);
        if ($http_code != 200) {
            // return http code and error message
            return json_encode([
                'code'    => $http_code,
                'message' => $data,
            ]);
        }
        return $data;
    }
}