<?php


namespace App\Middleware;

class DisplayInputErrorsMiddleware extends Middleware
{

   public function __invoke($request, $response, $next)
   {
       // TODO: Implement __invoke() method.
        if(isset($_SESSION['errors'])) {
            $this->container->view->getEnvironment()->addGlobal('erros', $_SESSION['errors']);
        }
        unset($_SESSION['errors']);
        $response = $next($request, $response);
        return $response;
   }
}