<?php

namespace App\Controllers;

use App\Controllers\PokeController;
use App\Controllers\ApiController;

class HomeController extends Controller

{

    public function index($request, $response)
    {


        if ($request->isGet()) {
            $offset = $request->getParam('offset');
            $limit = $request->getParam('limit');
            if ($limit > 20) {
                $limit = 20;
            }
            $pokemon = new PokeController;
            $dataPoke = $pokemon->resourceList(pokemon, $limit, $offset);
            $poked = json_decode($dataPoke);
            $pokemonList = $poked->results;

            //para paginação
            $pokemonNext = parse_url($poked->next, PHP_URL_QUERY);
            $pokemonPrevious = parse_url($poked->previous, PHP_URL_QUERY);

            $data = [
                'quantidade' => $poked->count,
                'pokelist' => $pokemonList,
                'next' => $pokemonNext,
                'previous' => $pokemonPrevious,
                'limit' => $limit,

            ];
            return $this->container->view->render($response, 'poke.twig', $data);

        }
    }

    public function pokemon($request, $response)
    {


        if ($request->isGet()) {
            $url = parse_url($request->getUri(), PHP_URL_PATH);
            $pokemon = new PokeController;
            $dataSingle = $pokemon->pokemon($url);

            $single = json_decode($dataSingle);

            $pagination = $this->pokePreviousNext($single->id);

            $data = [
                'id' => $single->id,
                'name' => ucfirst($single->name),
                'base_experience' => $single->base_experience,
                'height' => $single->height,
                'weight' => $single->weight,
                'abilities' => $single->abilities,
                'sprites' => $single->sprites,
                'types' => $single->types,
                'stats' => $single->stats,
                'pagination' => $pagination
                ];
                return $this->container->view->render($response, 'single.twig', $data);

        }

    }
    //se o id = 1
    //
    public function pokePreviousNext($id){

        if($id <= 0 || $id === 1){
            $pagination = array(
                'current' => $arr,
                'previous' => null,
                'next' => ++$id
            );
        }elseif($id > 1) {
            $pagination = array(
                'current' => $arr,
                'previous' => --$id,
                'next' => $id = $id + 2
            );

        }

        return $pagination;
    }



    public function pokeTotal(){
        $pokemon = new PokeController;
        $dataPoke = $pokemon->resourceList(pokemon, 1, 0);
        $poked = json_decode($dataPoke);



        return $poked->count;
    }

    public function singlePagination($id){

        $arr = array();

        $pokemon = new PokeController;
        $dataPoke = $pokemon->resourceList(pokemon, 1, 0);
        $data = json_decode($dataPoke);
        $pokemons = $data->results;

        foreach ( $pokemons as $pokemon )
        {

            $id1 = explode("/", $pokemon->url);
            $t = end($id1);

            $t = prev($id1) .' - '.$pokemon->name;


            if($id == $t){

                $current = $t;
                $previous = $t - 1;
                $next = $t + 2;
                $arr = array(
                    'current' => $current,
                    'previous' => $previous,
                    'next' => $next,
                );

                break;
            }

        }

        return $arr;
    }


}