<?php

    function getControllers($container, array $args){

        foreach ($args as $name) {
            $container[$name] = function ($container) use($name){
                $namespace = "App\\Controllers\\$name";

                return new $namespace($container);
            };
        }

    }
