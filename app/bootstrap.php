<?php

    session_start();

    date_default_timezone_set('America/Sao_Paulo');

    require __DIR__ . '/../vendor/autoload.php';

    $app = new Slim\App([
        'settings' =>[
            'displayErrorDetails' => true
        ]

    ]);

    $container = $app->getContainer();

    $container['view'] = function ($container){
        $view = new Slim\Views\Twig(__DIR__ . '/../resources/views', [
                'cache' => false,
            'debug' => true,
            ]);

        $view->addExtension(new \Slim\Views\TwigExtension(
                $container->router,
                $container->request->getUri()
        ));
        $view->addExtension(new \Twig\Extension\DebugExtension());

        return $view;
    };

    require __DIR__. '/commons.php';
    getControllers($container, [
        'HomeController',
        'PokeController',
        'ApiController',
    ]);

    $app->add(new App\Middleware\DisplayInputErrorsMiddleware($container));

    require __DIR__ . '/routes.php';